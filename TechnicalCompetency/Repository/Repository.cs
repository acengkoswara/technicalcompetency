﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TechnicalCompetency.Models;

namespace TechnicalCompetency.Repository
{
    public class DBRepository
    {
        private DataContext context;

        public DBRepository(DataContext ctx)
        {
            context = ctx;
        } 

        public IList<P_TC_COMPETENCY> GetAllCompetency()
        {
            var result = context.P_TC_COMPETENCY.Include(measurement => measurement.P_TC_MEASURE_TOOL).ToList();
            return result;
        }
    }
}

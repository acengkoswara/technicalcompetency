﻿templatingApp.controller('techcompController', ['$scope', '$http', function ($scope, $http) {
    $scope.title = "All Competency";
    $scope.ListUser = null;
    $scope.userModel = {};
    $scope.userModel.id = 0;
    getallData();

    //******=========Get All Competency=========******
    function getallData() {
        $http({
            method: 'GET',
            url: '/api/Values/GetCompetency/'
        }).then(function (response) {
            $scope.ListCompetency = response.data;
        }, function (error) {
            console.log(error);
        });
    };  
}]);
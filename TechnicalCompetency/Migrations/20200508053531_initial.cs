﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TechnicalCompetency.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "P_APP_ROLE",
                columns: table => new
                {
                    P_APP_ROLE_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CODE = table.Column<string>(nullable: true),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APP_ROLE", x => x.P_APP_ROLE_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_APPLICATION",
                columns: table => new
                {
                    P_APPLICATION_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CODE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APPLICATION", x => x.P_APPLICATION_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_ORGANIZATION_LEVEL",
                columns: table => new
                {
                    P_ORGANIZATION_LEVEL_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LEVEL_CODE = table.Column<string>(nullable: true),
                    LEVEL_NO = table.Column<long>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_ORGANIZATION_LEVEL", x => x.P_ORGANIZATION_LEVEL_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_TC_MEASURE_TOOL",
                columns: table => new
                {
                    P_TC_MEASURE_TOOL_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CODE = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_TC_MEASURE_TOOL", x => x.P_TC_MEASURE_TOOL_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_USER_STATUS",
                columns: table => new
                {
                    P_USER_STATUS_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CODE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_USER_STATUS", x => x.P_USER_STATUS_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_W_DOC_TYPE",
                columns: table => new
                {
                    P_W_DOC_TYPE_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DOC_NAME = table.Column<string>(nullable: true),
                    DISPLAY_NAME = table.Column<string>(nullable: true),
                    OWNER = table.Column<string>(nullable: true),
                    TDOC = table.Column<string>(nullable: true),
                    TCTL = table.Column<string>(nullable: true),
                    TUSER = table.Column<string>(nullable: true),
                    F_PROFILE = table.Column<string>(nullable: true),
                    PROFILE_SOURCE = table.Column<string>(nullable: true),
                    LISTING_NO = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true),
                    PACKAGE_NAME = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_DOC_TYPE", x => x.P_W_DOC_TYPE_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_W_INBOX",
                columns: table => new
                {
                    PROFILE_TYPE = table.Column<string>(nullable: true),
                    DISPLAY_NO = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "P_W_PROC",
                columns: table => new
                {
                    P_W_PROC_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PROC_NAME = table.Column<string>(nullable: true),
                    DISPLAY_NAME = table.Column<string>(nullable: true),
                    SEQNO = table.Column<int>(nullable: false),
                    F_BEFORE = table.Column<string>(nullable: true),
                    F_AFTER = table.Column<string>(nullable: true),
                    IS_ACTIVE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATE_DATE = table.Column<DateTime>(nullable: false),
                    CREATE_BY = table.Column<string>(nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_PROC", x => x.P_W_PROC_ID);
                });

            migrationBuilder.CreateTable(
                name: "P_APP_MENU",
                columns: table => new
                {
                    P_APP_MENU_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APPLICATION_ID = table.Column<long>(nullable: true),
                    CODE = table.Column<string>(nullable: true),
                    PARENT_ID = table.Column<long>(nullable: false),
                    FILE_NAME = table.Column<string>(nullable: true),
                    LISTING_NO = table.Column<long>(nullable: false),
                    IS_ACTIVE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APP_MENU", x => x.P_APP_MENU_ID);
                    table.ForeignKey(
                        name: "FK_P_APP_MENU_P_APPLICATION_P_APPLICATION_ID",
                        column: x => x.P_APPLICATION_ID,
                        principalTable: "P_APPLICATION",
                        principalColumn: "P_APPLICATION_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "P_APPLICATION_ROLE",
                columns: table => new
                {
                    P_APPLICATION_ROLE_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APP_ROLE_ID = table.Column<long>(nullable: false),
                    P_APPLICATION_ID = table.Column<long>(nullable: false),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APPLICATION_ROLE", x => x.P_APPLICATION_ROLE_ID);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_ROLE_P_APPLICATION_P_APPLICATION_ID",
                        column: x => x.P_APPLICATION_ID,
                        principalTable: "P_APPLICATION",
                        principalColumn: "P_APPLICATION_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_ROLE_P_APP_ROLE_P_APP_ROLE_ID",
                        column: x => x.P_APP_ROLE_ID,
                        principalTable: "P_APP_ROLE",
                        principalColumn: "P_APP_ROLE_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_ORGANIZATION",
                columns: table => new
                {
                    P_ORGANIZATION_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ORGANIZATION_CODE = table.Column<string>(nullable: true),
                    ORGANIZATION_NAME = table.Column<string>(nullable: true),
                    P_ORGANIZATION_LEVEL_ID = table.Column<long>(nullable: false),
                    LISTING_NO = table.Column<long>(nullable: false),
                    PARENT_ID = table.Column<long>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_ORGANIZATION", x => x.P_ORGANIZATION_ID);
                    table.ForeignKey(
                        name: "FK_P_ORGANIZATION_P_ORGANIZATION_LEVEL_P_ORGANIZATION_LEVEL_ID",
                        column: x => x.P_ORGANIZATION_LEVEL_ID,
                        principalTable: "P_ORGANIZATION_LEVEL",
                        principalColumn: "P_ORGANIZATION_LEVEL_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_TC_COMPETENCY",
                columns: table => new
                {
                    P_TC_COMPETENCY_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    COMPETENCY = table.Column<string>(nullable: true),
                    P_TC_MEASURE_TOOL_ID = table.Column<long>(nullable: false),
                    WEIGHT_COMPETENCY = table.Column<long>(nullable: false),
                    ENROLLMENT_KEY = table.Column<string>(nullable: true),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_TC_COMPETENCY", x => x.P_TC_COMPETENCY_ID);
                    table.ForeignKey(
                        name: "FK_P_TC_COMPETENCY_P_TC_MEASURE_TOOL_P_TC_MEASURE_TOOL_ID",
                        column: x => x.P_TC_MEASURE_TOOL_ID,
                        principalTable: "P_TC_MEASURE_TOOL",
                        principalColumn: "P_TC_MEASURE_TOOL_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_APP_USER",
                columns: table => new
                {
                    P_APP_USER_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_USER_STATUS_ID = table.Column<long>(nullable: false),
                    USER_NAME = table.Column<string>(nullable: true),
                    RAW_PWD = table.Column<string>(maxLength: 50, nullable: true),
                    PERNR = table.Column<long>(nullable: false),
                    FULL_NAME = table.Column<string>(nullable: true),
                    EMAIL_ADDRESS = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APP_USER", x => x.P_APP_USER_ID);
                    table.ForeignKey(
                        name: "FK_P_APP_USER_P_USER_STATUS_P_USER_STATUS_ID",
                        column: x => x.P_USER_STATUS_ID,
                        principalTable: "P_USER_STATUS",
                        principalColumn: "P_USER_STATUS_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_W_PROC_FILES",
                columns: table => new
                {
                    P_W_PROC_FILES_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_W_PROC_ID = table.Column<long>(nullable: true),
                    FILENAME = table.Column<string>(nullable: false),
                    IS_ACTIVE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATE_DATE = table.Column<DateTime>(nullable: false),
                    CREATE_BY = table.Column<string>(nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_PROC_FILES", x => x.P_W_PROC_FILES_ID);
                    table.ForeignKey(
                        name: "FK_P_W_PROC_FILES_P_W_PROC_P_W_PROC_ID",
                        column: x => x.P_W_PROC_ID,
                        principalTable: "P_W_PROC",
                        principalColumn: "P_W_PROC_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "P_W_WF",
                columns: table => new
                {
                    P_W_WF_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DOC_NAME = table.Column<string>(nullable: true),
                    DISPLAY_NAME = table.Column<string>(nullable: true),
                    P_W_DOC_TYPE_ID = table.Column<long>(nullable: false),
                    P_W_PROC_ID = table.Column<long>(nullable: true),
                    P_W_PROC_ID_START = table.Column<long>(nullable: false),
                    IS_ACTIVE = table.Column<string>(nullable: true),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_WF", x => x.P_W_WF_ID);
                    table.ForeignKey(
                        name: "FK_P_W_WF_P_W_DOC_TYPE_P_W_DOC_TYPE_ID",
                        column: x => x.P_W_DOC_TYPE_ID,
                        principalTable: "P_W_DOC_TYPE",
                        principalColumn: "P_W_DOC_TYPE_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_W_WF_P_W_PROC_P_W_PROC_ID",
                        column: x => x.P_W_PROC_ID,
                        principalTable: "P_W_PROC",
                        principalColumn: "P_W_PROC_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "P_APP_ROLE_MENU",
                columns: table => new
                {
                    P_APP_ROLE_MENU_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APP_ROLE_ID = table.Column<long>(nullable: false),
                    P_APP_MENU_ID = table.Column<long>(nullable: false),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APP_ROLE_MENU", x => x.P_APP_ROLE_MENU_ID);
                    table.ForeignKey(
                        name: "FK_P_APP_ROLE_MENU_P_APP_MENU_P_APP_MENU_ID",
                        column: x => x.P_APP_MENU_ID,
                        principalTable: "P_APP_MENU",
                        principalColumn: "P_APP_MENU_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_APP_ROLE_MENU_P_APP_ROLE_P_APP_ROLE_ID",
                        column: x => x.P_APP_ROLE_ID,
                        principalTable: "P_APP_ROLE",
                        principalColumn: "P_APP_ROLE_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_APP_USER_ROLE",
                columns: table => new
                {
                    P_APP_USER_ROLE_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APP_USER_ID = table.Column<long>(nullable: false),
                    P_APP_ROLE_ID = table.Column<long>(nullable: false),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APP_USER_ROLE", x => x.P_APP_USER_ROLE_ID);
                    table.ForeignKey(
                        name: "FK_P_APP_USER_ROLE_P_APP_ROLE_P_APP_ROLE_ID",
                        column: x => x.P_APP_ROLE_ID,
                        principalTable: "P_APP_ROLE",
                        principalColumn: "P_APP_ROLE_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_APP_USER_ROLE_P_APP_USER_P_APP_USER_ID",
                        column: x => x.P_APP_USER_ID,
                        principalTable: "P_APP_USER",
                        principalColumn: "P_APP_USER_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_W_CHART_PROC",
                columns: table => new
                {
                    P_W_CHART_PROC_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_W_WF_ID = table.Column<long>(nullable: false),
                    P_W_PROC_ID_PREV = table.Column<long>(nullable: false),
                    P_W_PROC_ID_NEXT = table.Column<long>(nullable: false),
                    P_W_PROC_ID_ALT = table.Column<long>(nullable: false),
                    IMPORTANCE_LEVEL = table.Column<string>(nullable: true),
                    F_INIT = table.Column<string>(nullable: true),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true),
                    CREATE_DATE = table.Column<DateTime>(nullable: false),
                    CREATE_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_CHART_PROC", x => x.P_W_CHART_PROC_ID);
                    table.ForeignKey(
                        name: "FK_P_W_CHART_PROC_P_W_WF_P_W_WF_ID",
                        column: x => x.P_W_WF_ID,
                        principalTable: "P_W_WF",
                        principalColumn: "P_W_WF_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_APPLICATION_USER_ROLE",
                columns: table => new
                {
                    P_APPLICATION_USER_ROLE_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APP_USER_ROLE_ID = table.Column<long>(nullable: false),
                    P_APPLICATION_ID = table.Column<long>(nullable: false),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APPLICATION_USER_ROLE", x => x.P_APPLICATION_USER_ROLE_ID);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_USER_ROLE_P_APPLICATION_P_APPLICATION_ID",
                        column: x => x.P_APPLICATION_ID,
                        principalTable: "P_APPLICATION",
                        principalColumn: "P_APPLICATION_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_USER_ROLE_P_APP_USER_ROLE_P_APP_USER_ROLE_ID",
                        column: x => x.P_APP_USER_ROLE_ID,
                        principalTable: "P_APP_USER_ROLE",
                        principalColumn: "P_APP_USER_ROLE_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "P_W_DAEMON_PROC",
                columns: table => new
                {
                    P_W_DAEMON_PROC_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_W_CHART_PROC_ID_ID = table.Column<long>(nullable: false),
                    P_W_CHART_PROC_ID = table.Column<long>(nullable: true),
                    DAEMON_NAME = table.Column<string>(nullable: true),
                    EXPRESSION_RULE = table.Column<string>(nullable: true),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: false),
                    UPDATE_BY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_W_DAEMON_PROC", x => x.P_W_DAEMON_PROC_ID);
                    table.ForeignKey(
                        name: "FK_P_W_DAEMON_PROC_P_W_CHART_PROC_P_W_CHART_PROC_ID",
                        column: x => x.P_W_CHART_PROC_ID,
                        principalTable: "P_W_CHART_PROC",
                        principalColumn: "P_W_CHART_PROC_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "P_APPLICATION_ROLE_MENU",
                columns: table => new
                {
                    P_APPLICATION_ROLE_MENU_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    P_APPLICATION_ROLE_ID = table.Column<long>(nullable: false),
                    P_APP_MENU_ID = table.Column<long>(nullable: false),
                    VALID_FROM = table.Column<DateTime>(nullable: false),
                    VALID_TO = table.Column<DateTime>(nullable: false),
                    DESCRIPTION = table.Column<string>(nullable: true),
                    CREATION_DATE = table.Column<DateTime>(nullable: false),
                    CREATED_BY = table.Column<string>(nullable: true),
                    UPDATED_DATE = table.Column<DateTime>(nullable: false),
                    UPDATED_BY = table.Column<string>(nullable: true),
                    P_APPLICATION_USER_ROLE_ID = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_P_APPLICATION_ROLE_MENU", x => x.P_APPLICATION_ROLE_MENU_ID);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_ROLE_MENU_P_APPLICATION_ROLE_P_APPLICATION_ROLE_ID",
                        column: x => x.P_APPLICATION_ROLE_ID,
                        principalTable: "P_APPLICATION_ROLE",
                        principalColumn: "P_APPLICATION_ROLE_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_ROLE_MENU_P_APPLICATION_USER_ROLE_P_APPLICATION_USER_ROLE_ID",
                        column: x => x.P_APPLICATION_USER_ROLE_ID,
                        principalTable: "P_APPLICATION_USER_ROLE",
                        principalColumn: "P_APPLICATION_USER_ROLE_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_P_APPLICATION_ROLE_MENU_P_APP_MENU_P_APP_MENU_ID",
                        column: x => x.P_APP_MENU_ID,
                        principalTable: "P_APP_MENU",
                        principalColumn: "P_APP_MENU_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_MENU_P_APPLICATION_ID",
                table: "P_APP_MENU",
                column: "P_APPLICATION_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_ROLE_MENU_P_APP_MENU_ID",
                table: "P_APP_ROLE_MENU",
                column: "P_APP_MENU_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_ROLE_MENU_P_APP_ROLE_ID",
                table: "P_APP_ROLE_MENU",
                column: "P_APP_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_USER_P_USER_STATUS_ID",
                table: "P_APP_USER",
                column: "P_USER_STATUS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_USER_ROLE_P_APP_ROLE_ID",
                table: "P_APP_USER_ROLE",
                column: "P_APP_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APP_USER_ROLE_P_APP_USER_ID",
                table: "P_APP_USER_ROLE",
                column: "P_APP_USER_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_ROLE_P_APPLICATION_ID",
                table: "P_APPLICATION_ROLE",
                column: "P_APPLICATION_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_ROLE_P_APP_ROLE_ID",
                table: "P_APPLICATION_ROLE",
                column: "P_APP_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_ROLE_MENU_P_APPLICATION_ROLE_ID",
                table: "P_APPLICATION_ROLE_MENU",
                column: "P_APPLICATION_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_ROLE_MENU_P_APPLICATION_USER_ROLE_ID",
                table: "P_APPLICATION_ROLE_MENU",
                column: "P_APPLICATION_USER_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_ROLE_MENU_P_APP_MENU_ID",
                table: "P_APPLICATION_ROLE_MENU",
                column: "P_APP_MENU_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_USER_ROLE_P_APPLICATION_ID",
                table: "P_APPLICATION_USER_ROLE",
                column: "P_APPLICATION_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_APPLICATION_USER_ROLE_P_APP_USER_ROLE_ID",
                table: "P_APPLICATION_USER_ROLE",
                column: "P_APP_USER_ROLE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_ORGANIZATION_P_ORGANIZATION_LEVEL_ID",
                table: "P_ORGANIZATION",
                column: "P_ORGANIZATION_LEVEL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_TC_COMPETENCY_P_TC_MEASURE_TOOL_ID",
                table: "P_TC_COMPETENCY",
                column: "P_TC_MEASURE_TOOL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_W_CHART_PROC_P_W_WF_ID",
                table: "P_W_CHART_PROC",
                column: "P_W_WF_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_W_DAEMON_PROC_P_W_CHART_PROC_ID",
                table: "P_W_DAEMON_PROC",
                column: "P_W_CHART_PROC_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_W_PROC_FILES_P_W_PROC_ID",
                table: "P_W_PROC_FILES",
                column: "P_W_PROC_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_W_WF_P_W_DOC_TYPE_ID",
                table: "P_W_WF",
                column: "P_W_DOC_TYPE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_P_W_WF_P_W_PROC_ID",
                table: "P_W_WF",
                column: "P_W_PROC_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "P_APP_ROLE_MENU");

            migrationBuilder.DropTable(
                name: "P_APPLICATION_ROLE_MENU");

            migrationBuilder.DropTable(
                name: "P_ORGANIZATION");

            migrationBuilder.DropTable(
                name: "P_TC_COMPETENCY");

            migrationBuilder.DropTable(
                name: "P_W_DAEMON_PROC");

            migrationBuilder.DropTable(
                name: "P_W_INBOX");

            migrationBuilder.DropTable(
                name: "P_W_PROC_FILES");

            migrationBuilder.DropTable(
                name: "P_APPLICATION_ROLE");

            migrationBuilder.DropTable(
                name: "P_APPLICATION_USER_ROLE");

            migrationBuilder.DropTable(
                name: "P_APP_MENU");

            migrationBuilder.DropTable(
                name: "P_ORGANIZATION_LEVEL");

            migrationBuilder.DropTable(
                name: "P_TC_MEASURE_TOOL");

            migrationBuilder.DropTable(
                name: "P_W_CHART_PROC");

            migrationBuilder.DropTable(
                name: "P_APP_USER_ROLE");

            migrationBuilder.DropTable(
                name: "P_APPLICATION");

            migrationBuilder.DropTable(
                name: "P_W_WF");

            migrationBuilder.DropTable(
                name: "P_APP_ROLE");

            migrationBuilder.DropTable(
                name: "P_APP_USER");

            migrationBuilder.DropTable(
                name: "P_W_DOC_TYPE");

            migrationBuilder.DropTable(
                name: "P_W_PROC");

            migrationBuilder.DropTable(
                name: "P_USER_STATUS");
        }
    }
}

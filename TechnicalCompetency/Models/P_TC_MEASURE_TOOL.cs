﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
    public class P_TC_MEASURE_TOOL
    {
		public long P_TC_MEASURE_TOOL_ID { set; get; }
		public string CODE { get; set; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
    public partial class P_W_PROC
    {
		public long P_W_PROC_ID { set; get; }
		public string PROC_NAME { set; get; }
		public string DISPLAY_NAME { set; get; }
		public int SEQNO { set; get; }
		public string F_BEFORE { set; get; }
		public string F_AFTER { set; get; }
		public string IS_ACTIVE { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATE_DATE { set; get; }
		public string CREATE_BY { set; get; }
		public DateTime UPDATE_DATE { set; get; }
		public string UPDATE_BY { set; get; }
		public ICollection<P_W_PROC_FILES> P_W_PROC_FILES { get; set; }

	}
}

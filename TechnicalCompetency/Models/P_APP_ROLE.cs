﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_APP_ROLE
    {
		public long P_APP_ROLE_ID { get; set; }
        public string CODE { get; set; }
		public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
		public string DESCRIPTION { get; set; }
		public DateTime CREATION_DATE { get; set; }
	    public string CREATED_BY { get; set; }
        public DateTime UPDATED_DATE { get; set; } 
	    public string UPDATED_BY { get; set; }
        public ICollection<P_APPLICATION_ROLE> P_APPLICATION_ROLE { get; set; }
        public ICollection<P_APP_USER_ROLE> P_APP_USER_ROLE { get; set; }
    }
}

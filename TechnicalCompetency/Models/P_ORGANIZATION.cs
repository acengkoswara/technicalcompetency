﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_ORGANIZATION
    {
		public long P_ORGANIZATION_ID { set; get; } 
	    public string ORGANIZATION_CODE { set; get; }
		public string ORGANIZATION_NAME { set; get; }
		[ForeignKey("P_ORGANIZATION_LEVEL")]
		public long P_ORGANIZATION_LEVEL_ID { get; set; }
		public P_ORGANIZATION_LEVEL P_ORGANIZATION_LEVEL { get; set; }
		public long LISTING_NO { set; get; }
		public long PARENT_ID { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
	}
}

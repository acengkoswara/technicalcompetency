﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_W_PROC_FILES
    {
		public long P_W_PROC_FILES_ID { set; get; }
		public P_W_PROC P_W_PROC { get; set; }
		[Required]
		public string FILENAME { set; get; }
		long SEQUENCE_NO { set; get; }
		public string IS_ACTIVE { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATE_DATE { set; get; }
		public string CREATE_BY { set; get; }
		public DateTime UPDATE_DATE { set; get; }
		public string UPDATE_BY { set; get; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public partial class P_W_WF
    {
        public long P_W_WF_ID { set; get; }
        public string DOC_NAME { set; get; }
        public string DISPLAY_NAME { set; get; }
        [ForeignKey("P_W_DOC_TYPE")]
        public long P_W_DOC_TYPE_ID { set; get; }
        public P_W_DOC_TYPE P_W_DOC_TYPE { get; set; }
        public P_W_PROC P_W_PROC { get; set; }
        public long P_W_PROC_ID_START { set; get; }
        public string IS_ACTIVE { set; get; }
        public string DESCRIPTION { set; get; }
        public DateTime UPDATE_DATE { set; get; }
        public string UPDATE_BY { set; get; }
    } 
}

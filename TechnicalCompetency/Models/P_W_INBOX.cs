﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
    public class P_W_INBOX
    {
        public string PROFILE_TYPE { set; get; }
        public string DISPLAY_NO { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
    public class P_ORGANIZATION_LEVEL
    {
		public long P_ORGANIZATION_LEVEL_ID { set; get; }
		public string LEVEL_CODE { set; get; }
		public long LEVEL_NO { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TechnicalCompetency.Models
{
    public class P_W_DAEMON_PROC
    {
        public long P_W_DAEMON_PROC_ID { set; get; }
        [ForeignKey("[P_W_CHART_PROC_ID")]
        public long P_W_CHART_PROC_ID_ID { set; get; }
        public P_W_CHART_PROC P_W_CHART_PROC { get; set; }
        public string DAEMON_NAME { set; get; }
        public string EXPRESSION_RULE { set; get; }
        public DateTime VALID_FROM { set; get; }
        public DateTime VALID_TO { set; get; }
        public string DESCRIPTION { set; get; }
        public DateTime UPDATE_DATE { set; get; }
        public string UPDATE_BY { set; get; }
    }
}

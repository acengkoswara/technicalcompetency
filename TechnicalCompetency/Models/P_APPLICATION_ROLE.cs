﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_APPLICATION_ROLE
    {
		public long P_APPLICATION_ROLE_ID { get; set; }
		[ForeignKey("P_APP_ROLE")]
		public long P_APP_ROLE_ID { get; set; }
		public P_APP_ROLE P_APP_ROLE { get; set; }
		[ForeignKey("P_APPLICATION")]
		public long P_APPLICATION_ID { set; get; }
		public P_APPLICATION P_APPLICATION { get; set; }
		public DateTime VALID_FROM { set; get; }
		public DateTime VALID_TO { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
		public IList<P_APPLICATION_ROLE_MENU> ROLE_MENU { set; get; }
	}
}

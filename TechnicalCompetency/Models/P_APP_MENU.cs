﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_APP_MENU
    {
		public long P_APP_MENU_ID { get; set; }
		public P_APPLICATION P_APPLICATION { get; set; }
	    public string CODE { get; set; }
		public long PARENT_ID { get; set; }
		public string FILE_NAME { get; set; }
		public long LISTING_NO { get; set; }
		public string IS_ACTIVE { get; set; }
		public string DESCRIPTION { get; set; }
		public DateTime CREATION_DATE { get; set; }
		public string CREATED_BY { get; set; }
	    public DateTime UPDATED_DATE { get; set; }
	}
}

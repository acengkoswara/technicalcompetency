﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_TC_COMPETENCY
    {
		public long P_TC_COMPETENCY_ID { set; get; }
		public string COMPETENCY { get; set; }
		[ForeignKey("P_TC_MEASURE_TOOL")]
		public long P_TC_MEASURE_TOOL_ID { set; get; }
		public P_TC_MEASURE_TOOL P_TC_MEASURE_TOOL { get; set; }
		public long WEIGHT_COMPETENCY { set; get; }
		public string ENROLLMENT_KEY { get; set; }
		public DateTime VALID_FROM { set; get; }
		public DateTime VALID_TO { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
	}
}

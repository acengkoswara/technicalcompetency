﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_W_CHART_PROC
    {
		public long P_W_CHART_PROC_ID { set; get; }
		[ForeignKey("P_W_WF")]
		public long P_W_WF_ID { set; get; }
		public P_W_WF P_W_WF { get; set; }
		public long P_W_PROC_ID_PREV { set; get; }
		public long P_W_PROC_ID_NEXT { set; get; }
		public long P_W_PROC_ID_ALT { set; get; }
		public string IMPORTANCE_LEVEL { set; get; }
		public string F_INIT { set; get; }
		public DateTime VALID_FROM { set; get; }
		public DateTime VALID_TO { set; get; }
		public DateTime UPDATE_DATE { set; get; }
		public string UPDATE_BY { set; get; }
		public DateTime CREATE_DATE { set; get; }
		public string CREATE_BY { set; get; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
    public class P_APPLICATION
    {
		public long P_APPLICATION_ID { set; get; }
		public string CODE { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
		public IList<P_APPLICATION_ROLE> ROLE { get; set; }		
	}
	
}

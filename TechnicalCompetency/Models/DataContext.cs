﻿using Microsoft.EntityFrameworkCore;


namespace TechnicalCompetency.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> opts)
            : base(opts) { }

        public DbSet<P_APP_MENU> P_APP_MENU { get; set; }
        public DbSet<P_APP_ROLE> P_APP_ROLE { get; set; }
        public DbSet<P_APP_ROLE_MENU> P_APP_ROLE_MENU { get; set; }
        public DbSet<P_APP_USER> P_APP_USER { get; set; }
        public DbSet<P_APP_USER_ROLE> P_APP_USER_ROLE { get; set; }
        public DbSet<P_APPLICATION> P_APPLICATION { get; set; }
        public DbSet<P_APPLICATION_ROLE> P_APPLICATION_ROLE { get; set; }
        public DbSet<P_ORGANIZATION> P_ORGANIZATION { get; set; }
        public DbSet<P_ORGANIZATION_LEVEL> P_ORGANIZATION_LEVEL { get; set; }
        public DbSet<P_USER_STATUS> P_USER_STATUS { get; set; }
        public DbSet<P_W_CHART_PROC> P_W_CHART_PROC { get; set; }
        public DbSet<P_W_DAEMON_PROC> P_W_DAEMON_PROC { get; set; }
        public DbSet<P_W_DOC_TYPE> P_W_DOC_TYPE { get; set; }
        public DbSet<P_W_INBOX> P_W_INBOX { get; set; }
        public DbSet<P_W_PROC> P_W_PROC { get; set; }
        public DbSet<P_W_PROC_FILES> P_W_PROC_FILES { get; set; }
        public DbSet<P_W_WF> P_W_WF { get; set; }
        public DbSet<P_APPLICATION_ROLE_MENU> P_APPLICATION_ROLE_MENU { get; set; }
        public DbSet<P_APPLICATION_USER_ROLE> P_APPLICATION_USER_ROLE { get; set; }
        public DbSet<P_TC_COMPETENCY> P_TC_COMPETENCY { get; set; }
        public DbSet<P_TC_MEASURE_TOOL> P_TC_MEASURE_TOOL { get; set; }        


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<P_APP_MENU>().HasKey(e => e.P_APP_MENU_ID);
            modelBuilder.Entity<P_APP_ROLE>().HasKey(e => e.P_APP_ROLE_ID);
            modelBuilder.Entity<P_APP_ROLE_MENU>().HasKey(e => e.P_APP_ROLE_MENU_ID);
            modelBuilder.Entity<P_APP_USER>().HasKey(e => e.P_APP_USER_ID);
            modelBuilder.Entity<P_APP_USER_ROLE>().HasKey(e => e.P_APP_USER_ROLE_ID);
            modelBuilder.Entity<P_APPLICATION>().HasKey(e => e.P_APPLICATION_ID);
            modelBuilder.Entity<P_APPLICATION_ROLE>().HasKey(e => e.P_APPLICATION_ROLE_ID);
            modelBuilder.Entity<P_ORGANIZATION>().HasKey(e => e.P_ORGANIZATION_ID);
            modelBuilder.Entity<P_ORGANIZATION_LEVEL>().HasKey(e => e.P_ORGANIZATION_LEVEL_ID);
            modelBuilder.Entity<P_USER_STATUS>().HasKey(e => e.P_USER_STATUS_ID);
            modelBuilder.Entity<P_W_CHART_PROC>().HasKey(e => e.P_W_CHART_PROC_ID);
            modelBuilder.Entity<P_W_DAEMON_PROC>().HasKey(e => e.P_W_DAEMON_PROC_ID);
            modelBuilder.Entity<P_W_DOC_TYPE>().HasKey(e => e.P_W_DOC_TYPE_ID);
            modelBuilder.Entity<P_W_PROC>().HasKey(e => e.P_W_PROC_ID);
            modelBuilder.Entity<P_W_PROC_FILES>().HasKey(e => e.P_W_PROC_FILES_ID);
            modelBuilder.Entity<P_W_WF>().HasKey(e => e.P_W_WF_ID);
            modelBuilder.Entity<P_W_INBOX>().HasNoKey();
            modelBuilder.Entity<P_APPLICATION_ROLE_MENU>().HasKey(e => e.P_APPLICATION_ROLE_MENU_ID);
            modelBuilder.Entity<P_APPLICATION_USER_ROLE>().HasKey(e => e.P_APPLICATION_USER_ROLE_ID);
            modelBuilder.Entity<P_TC_COMPETENCY>().HasKey(e => e.P_TC_COMPETENCY_ID);
            modelBuilder.Entity<P_TC_MEASURE_TOOL>().HasKey(e => e.P_TC_MEASURE_TOOL_ID);
        }
    }

}


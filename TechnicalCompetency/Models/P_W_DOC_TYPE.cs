﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnicalCompetency.Models
{
	public partial class P_W_DOC_TYPE
	{
		public long P_W_DOC_TYPE_ID { set; get; }
		public string DOC_NAME { set; get; }
		public string DISPLAY_NAME { set; get; }
		public string OWNER { set; get; }
		public string TDOC { set; get; }
		public string TCTL { set; get; }
		public string TUSER { set; get; }
		public string F_PROFILE { set; get; }
		public string PROFILE_SOURCE { set; get; }
		public string LISTING_NO { set; get; }
		public string DESCRIPTION { set; get; }
		public DateTime UPDATE_DATE { set; get; }
		public string UPDATE_BY { set; get; }
		public string PACKAGE_NAME { set; get; }
	}
}

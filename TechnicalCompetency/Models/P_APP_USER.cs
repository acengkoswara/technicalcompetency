﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnicalCompetency.Models
{
    public class P_APP_USER
    {
		public long P_APP_USER_ID { set; get; }
		[ForeignKey("P_USER_STATUS")]
		public long P_USER_STATUS_ID { set; get; }
		public P_USER_STATUS P_USER_STATUS { get; set; }
		public string USER_NAME { get; set; }
		[MaxLength(50)]
		public string RAW_PWD { set; get; }
		public long PERNR { set; get; }
		public string FULL_NAME { set; get; }
		public string EMAIL_ADDRESS { set; get; }
		public DateTime CREATION_DATE { set; get; }
		public string CREATED_BY { set; get; }
		public DateTime UPDATED_DATE { set; get; }
		public string UPDATED_BY { set; get; }
		public ICollection<P_APP_USER_ROLE> USER_ROLE { get; set; }
	}
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using Microsoft.EntityFrameworkCore;
using TechnicalCompetency.Models;

namespace TechnicalCompetency
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc();
            //services.AddCors(o => o.AddPolicy("AppPolicy", builder =>
            //{
            //    builder.AllowAnyOrigin()
            //           .AllowAnyMethod()
            //           .AllowAnyHeader();
            //}));
            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString));
            services.AddControllersWithViews();
            //services.AddRazorPages();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseCors();
            app.UseHttpsRedirection();
            app.UseStaticFiles();            
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            //SeedData.SeedDatabase(services.GetRequiredService<DataContext>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services)
        //{
        //    if (env.IsDevelopment())
        //    {
        //       app.UseDeveloperExceptionPage();
        //    }

        //    app.Use(async (context, next) =>
        //    {
        //        await next();
        //        if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
        //        {
        //            context.Request.Path = "/index.html";
        //            context.Response.StatusCode = 200;
        //            await next();
        //        }
        //    });

        //    DefaultFilesOptions options = new DefaultFilesOptions();
        //    options.DefaultFileNames.Clear();
        //    options.DefaultFileNames.Add("/index.html");
        //    app.UseDefaultFiles(options);

        //    app.UseStaticFiles();
        //    app.UseFileServer(enableDirectoryBrowsing: false);
        //    app.UseRouting();
        //    app.UseAuthorization();
        //    app.UseEndpoints(endpoints =>
        //    {
        //        endpoints.MapRazorPages();
        //    });
        //}
    }
}

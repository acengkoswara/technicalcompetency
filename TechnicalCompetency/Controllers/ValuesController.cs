﻿using Microsoft.AspNetCore.Mvc;
using TechnicalCompetency.Models;
using TechnicalCompetency.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Cors;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;

namespace TechnicalCompetency.Controllers
{
    [Route("api/Values"), Produces("application/json")]
    [ApiController]
    public class ValuesController : Controller
    {
        private DataContext context;
        public ValuesController(DataContext ctx)
        {
            context = ctx;
        }
        // GET: api/Values/GetCompetency
        [HttpGet, Route("GetCompetency")]
        public async Task<object> GetCompetency()
        {
            List<P_TC_COMPETENCY> competencys = null;
            DBRepository repoCompetency = new DBRepository(context);
            //    var result = userStart.GetUserApplication(user_login);
            object result = null;
            try
            {
                using (context)
                {
                    competencys = await context.P_TC_COMPETENCY.Include(measurement => measurement.P_TC_MEASURE_TOOL).ToListAsync();
                    result = new
                    {
                        User
                    };
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return competencys;
        }        
    }
}